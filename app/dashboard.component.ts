import { Component, OnInit } from '@angular/core';
import {Hero} from './hero';
import{HeroService} from './hero.service';
import {Router} from '@angular/router';

@Component({
  selector: 'my-dashboard',
  templateUrl: 'app/html/dashboard.component.html',
  styleUrls: ['app/css/dashboard.component.css']
})
export class DashboardComponent implements OnInit{
    ngOnInit(){
        this.heroService.getHeroes().then(heroes=>this.heroes=heroes.slice(2,6));
    }
    heroes: Hero[]=[];
    constructor(private heroService: HeroService,private router:Router){}

    gotoDetail(hero:Hero){
        let link=['/detail',hero.id];
        this.router.navigate(link);
    }
 }