import { Component, OnInit,Output,EventEmitter } from '@angular/core';
import { Hero } from './hero';
import { HeroDetailComponent } from './hero-detail.component';
import { MyHeroComponent } from './my-hero.component';
import {HeroService} from './hero.service';
import {Router} from '@angular/router';

@Component({
  selector: 'my-heroes',
  templateUrl: 'app/html/heroes.component.html',
  styleUrls: ['app/css/heroes.component.css'],
  directives: [HeroDetailComponent, MyHeroComponent]
})//@Component


export class HeroesComponent implements OnInit {
  title = 'Tour of Heroes';
  selectedHero: Hero;
  realHero: Hero;
  addingHero = false;
  heroes: Hero[];
  error: any;
  @Output() setMyHero= new EventEmitter();

  ngOnInit() { //calling at the right time by Angular
    this.getHeroes();
    this.setMyHero.emit(this.realHero);
  }
  onSelect(hero: Hero) { this.selectedHero = hero; }
  setNewHero() {
    this.realHero = this.selectedHero;
    let link = ['/realHero',this.realHero.id];
    this.router.navigate(link);
  }
  constructor(private heroService: HeroService, private router: Router) {  
   }
  getHeroes() {
    //this.heroes=this.heroService.getHeroes(); synchronicznie//
    this.heroService.getHeroes().then(heroes => this.heroes = heroes);  //asynchronicznie z Promise
  }
  gotoDetail() {
    let link = ['/detail', this.selectedHero.id];
    this.router.navigate(link);
  }
  addHero() {
    this.addingHero = true;
    this.selectedHero = null;
  }

  close(savedHero: Hero) {
    this.addingHero = false;
    if (savedHero) { this.getHeroes(); }
  }

  deleteHero(hero: Hero, event: any) {
    event.stopPropagation();
    this.heroService
      .delete(hero)
      .then(res => {
        this.heroes = this.heroes.filter(h => h !== hero);
        if (this.selectedHero === hero) { this.selectedHero = null; }
      })
      .catch(error => this.error = error);
  }

}




