import {Component } from '@angular/core';
import { ROUTER_DIRECTIVES} from '@angular/router';
import {HeroService} from './hero.service';
import {LoginComponent} from './login.component'
import {UserService} from './user.service'

@Component({
    selector: 'my-app',
    template: `<h1>{{title}}</h1>
    <login-comp></login-comp>
    <nav>
        <a [routerLink]="['/dashboard']" routerLinkActive="active">Dashboard</a>
        <a [routerLink]="['/heroes']">Heroes</a>
    </nav>
    
    <router-outlet></router-outlet>`,
    styleUrls:['app/css/app.component.css'],
    directives: [ROUTER_DIRECTIVES,LoginComponent],
    providers: [HeroService,UserService]

})

export class AppComponent {
    title = "Tour of Heroes (shell)";
}