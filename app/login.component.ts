import { Component, EventEmitter, Input, OnInit, OnDestroy, Output } from '@angular/core';
//import { Hero } from './hero';
//import {Route} from '@angular/router';
import {UserService} from './user.service';
import {CookieService} from 'angular2-cookie/core';

@Component({
  selector: 'login-comp',
  templateUrl: 'app/html/login.component.html',
  styleUrls: ['app/css/login.component.css'],
  providers: [CookieService]
})

export class LoginComponent implements OnInit, OnDestroy {
  error: any;
  navigated = false;
  login: string;
  password: string;
  logged: boolean = false;
  userName: string;


  constructor(private userService: UserService, private cookie: CookieService) { }

  ngOnInit() {
  }

  ngOnDestroy() {
  }

  tryLog() {
    this.userService.checkLog(this.login, this.password)
      .then(log => (this.cookie.put('isLog', log ? 'true' : 'false')));
    this.userService.getUserName(this.login).then(name => (this.cookie.put('userName', name)));;
    //this.cookie.put('isLog', this.logged ? 'true' : 'false');
    this.userService.getUserID(this.login).then(id=>this.cookie.put('userID',id.toString()));
    //localStorage.setItem('UserID','identyfikator user\'a');
  }

  logOut() {
    this.logged = false;
    this.cookie.removeAll();
  }

}