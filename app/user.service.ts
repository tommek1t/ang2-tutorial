import {Injectable} from '@angular/core';
//import {Hero} from './hero';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/toPromise'; //bo brak toPromise w http
import {User} from './user';


@Injectable()
export class UserService {

    private userUrl = 'app/users';  //url obecnie do let=users... w db
    constructor(private http: Http) { }
     getAllUsers(): Promise<User[]> {
        return this.http.get(this.userUrl) //zwraca observable
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);
    }

     getUser(login: string) {
        return this.getAllUsers().then(users => users.find(user => user.login === login));
    }

    getUserByID(id:number) {
        return this.getAllUsers().then(users => users.find(user => user.id === id));
    }

    checkLog(login: string, password: string) {
        return this.getUser(login).then(user => (user.passwd == password));
    }

    getUserName(login: string){
        return this.getUser(login).then(user => (user.name));
    }

    getUserID(login: string){
        return this.getUser(login).then(user => (user.id));
    }

    private handleError(error: any) {
        console.error('Wystapil blad!! (userService)', error);
        return Promise.reject(error.message || error);
    }

    /* private post(hero: Hero): Promise<Hero> {//add Hero
         let headers = new Headers({
             'Content-Type': 'application/json'
         });
 
         return this.http
             .post(this.heroesUrl, JSON.stringify(hero), { headers: headers })
             .toPromise()
             .then(res => res.json().data)
             .catch(this.handleError);
     }
 
     // Update existing Hero
     private put(hero: Hero) {
         let headers = new Headers();
         headers.append('Content-Type', 'application/json');
 
         let url = `${this.heroesUrl}/${hero.id}`;
 
         return this.http
             .put(url, JSON.stringify(hero), { headers: headers })
             .toPromise()
             .then(() => hero)
             .catch(this.handleError);
     }
 
     delete(hero: Hero) {
         let headers = new Headers();
         headers.append('Content-Type', 'application/json');
 
         let url = `${this.heroesUrl}/${hero.id}`;
 
         return this.http
             .delete(url, headers)
             .toPromise()
             .catch(this.handleError);
     }
 
     save(hero: Hero): Promise<Hero> {
         if (hero.id) {
             return this.put(hero);//update
         }
         return this.post(hero);//add new Hero
     }*/

}