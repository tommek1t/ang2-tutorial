export class InMemoryDataService {
  createDb() {

     let users = [
      {id: 111, login: 'ada1', passwd: 'ada2',name:'Adrianna'},
      {id: 120, login: 'mikolaj1', passwd: 'mikolaj2',name:'Mikolajek'},
      {id: 130, login: 'microbus', passwd: 'microbus2',name:'Firma przewozowa'},
      {id: 140, login: 'eddiye', passwd: 'eddiye2',name:'Eddie'}
    ];

    let heroes = [
      {id: 11, name: 'Mr. Nice', age: 78},
      {id: 12, name: 'Narco', age: 79},
      {id: 13, name: 'Bombasto', age: 48},
      {id: 14, name: 'Celeritas', age: 97},
      {id: 15, name: 'Magneta', age: 68},
      {id: 16, name: 'RubberMan', age: 76},
      {id: 17, name: 'Dynama', age: 65},
      {id: 18, name: 'Dr IQ', age: 103},
      {id: 19, name: 'Magma', age: 77},
      {id: 20, name: 'Tornado', age: 84}
    ];

   

    return {users,heroes};
  }
}