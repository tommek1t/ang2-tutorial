import {Injectable} from '@angular/core';
import {Hero} from './hero';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/toPromise'; //bo brak toPromise w http


@Injectable()
export class HeroService {

    private heroesUrl = 'app/heroes';  // URL to web api
    constructor(private http: Http) { }
    getHeroes(): Promise<Hero[]> {
        return this.http.get(this.heroesUrl) //zwraca observable
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);
    }

    getHeroesWithout(hero: Hero): Promise<Hero[]> {//ToDO: nie uwzglednia nowych
        var heroes:Promise<Hero[]>;
        heroes=this.http.get(this.heroesUrl)
            .toPromise()
            .then(response => response.json().data)
            heroes.then(heroes=> heroes.splice(heroes.indexOf(hero),1));

        return heroes;
    }


    getHero(id: number) {
        return this.getHeroes().then(heroes => heroes.find(hero => hero.id == id));
    }

    private handleError(error: any) {
        console.error('Blad z HeroService', error);
        return Promise.reject(error.message || error);
    }

    private post(hero: Hero): Promise<Hero> {//add Hero
        let headers = new Headers({
            'Content-Type': 'application/json'
        });

        return this.http
            .post(this.heroesUrl, JSON.stringify(hero), { headers: headers })
            .toPromise()
            .then(res => res.json().data)
            .catch(this.handleError);
    }

    // Update existing Hero
    private put(hero: Hero) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.heroesUrl}/${hero.id}`;

        return this.http
            .put(url, JSON.stringify(hero), { headers: headers })
            .toPromise()
            .then(() => hero)
            .catch(this.handleError);
    }

    delete(hero: Hero) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.heroesUrl}/${hero.id}`;

        return this.http
            .delete(url, headers)
            .toPromise()
            .catch(this.handleError);
    }

    save(hero: Hero): Promise<Hero> {
        if (hero.id) {
            return this.put(hero);//update
        }
        return this.post(hero);//add new Hero
    }

}