import { provideRouter, RouterConfig }  from '@angular/router';
import { HeroesComponent } from './heroes.component';
import {DashboardComponent} from './dashboard.component';
import {HeroDetailComponent} from './hero-detail.component';
import {MyHeroComponent} from './my-hero.component';
import {LoginComponent} from './login.component';

const routes: RouterConfig = [
  {
    path: 'heroes',
    component: HeroesComponent
  },

  {
      path:'dashboard',
      component: DashboardComponent
  },
  {
      path:'',
      redirectTo: '/dashboard',
      pathMatch: 'full'
  },
  {
      path:'detail/:id',
      component:HeroDetailComponent
  }
  ,
  {
    path:'realHero/:id',
    component:MyHeroComponent
  },
  {
    path: 'users',
    component: LoginComponent
  },
];

export const APP_ROUTER_PROVIDERS = [
  provideRouter(routes)
];
