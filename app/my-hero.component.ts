import { Component, OnInit, Input } from '@angular/core';
import {Hero} from './hero';
import {HeroService} from './hero.service';
import {Router} from '@angular/router';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'my-hero',
    templateUrl: 'app/html/my-hero.component.html',
    styleUrls: ['app/css/heroes.component.css']
})
export class MyHeroComponent implements OnInit {
    myHero: Hero;
    sub: any;
    idHero: number;
    smallHeroes: Hero[];
    @Input() hero: Hero;

    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
            this.idHero = +params['id'];
            this.heroService.getHero(this.idHero)
                .then(hero => this.myHero = hero).then
            this.heroService.getHeroesWithout(this.myHero).then(
                heroes => this.smallHeroes = heroes);
        });
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    goBack() {
        window.history.back();
    }

    constructor(private heroService: HeroService,
        private route: ActivatedRoute) { }
}
